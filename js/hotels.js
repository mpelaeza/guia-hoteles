$(function(){
		$('#exampleModal').on('show.bs.modal', function(event){
			$('#add-comment-btn').removeClass('btn-primary')
			console.log('se muestra el modal')

		})
		$('#exampleModal').on('shown.bs.modal', function(event){
			$('#add-comment-btn').prop('disabled', true)
			console.log('se mostró el modal')			
		})

		$('#exampleModal').on('hide.bs.modal', function(event){
			$('#add-comment-btn').addClass('btn-primary')
			console.log('se oculta el modal')			
		})

		$('#exampleModal').on('hidden.bs.modal', function(event){
			$('#add-comment-btn').prop('disabled', false)
			console.log('se ocultó el modal')			
		})
	})